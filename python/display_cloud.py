#!/usr/bin/env python3
import base64

import roslibpy
import numpy as np
import pylab as plt


def receive_cloud(msg):
    print(msg['height'])
    print(msg['width'])
    print(msg['fields'])
    print(msg['is_bigendian'])
    print(msg['point_step'])
    print(msg['row_step'])
    print(msg['is_dense'])
    # ascii_encoded = msg['data'].encode('ascii')
    # base64_decoded = base64.b64decode(ascii_encoded)
    #
    # ints = np.frombuffer(base64_decoded, dtype=np.uint8)
    # mat = np.reshape(ints, (1216, 1920))
    # plt.imshow(mat, cmap="gray")
    # plt.show()


if __name__ == "__main__":

    client = roslibpy.Ros(host='localhost', port=8082)
    client.run()

    print(client.get_topics())

    listener = roslibpy.Topic(client, '/point2', 'sensor_msgs/PointCloud2')
    listener.subscribe(receive_cloud)

    try:
        while True:
            pass
    except KeyboardInterrupt:
        client.terminate()
