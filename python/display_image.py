#!/usr/bin/env python3
import base64
import roslibpy
import numpy as np
import pylab as plt
import time
from itertools import count

start = time.time()
msg_count = count()
next(msg_count)

def receive_image(msg):
    print(str(next(msg_count)) + " images received in " + str(time.time() - start) + " seconds")
    ascii_encoded = msg['data'].encode('ascii')
    base64_decoded = base64.b64decode(ascii_encoded)

    ints = np.frombuffer(base64_decoded, dtype=np.uint8)
    mat = np.reshape(ints, (1216, 1920))
    plt.imshow(mat, cmap="gray")
    plt.show()


if __name__ == "__main__":

    client = roslibpy.Ros(host='localhost', port=8082)
    client.run()

    listener = roslibpy.Topic(client, '/left/image_raw', 'sensor_msgs/Image')
    start = time.time()
    listener.subscribe(receive_image)

    try:
        while True:
            pass
    except KeyboardInterrupt:
        client.terminate()
