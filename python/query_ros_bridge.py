#!/usr/bin/env python3

import roslibpy

if __name__ == "__main__":
    client = roslibpy.Ros(host='localhost', port=8082)
    client.run()

    print('Is ROS connected? ', client.is_connected)

    listener = roslibpy.Topic(client, '/left/camera_info', 'sensor_msgs/CameraInfo')
    listener.subscribe(lambda message: print('Got left camera info: %s ' % message))

    try:
        while True:
            pass
    except KeyboardInterrupt:
        client.terminate()
