#!/bin/bash

## 3/2022 : Revised bagfile with disparity, pointclouds
DEFAULT_BAG=trisect_emulator_sample_pointcloud.bag
DOWNLOAD_LOCATION=https://s3.us-west-1.wasabisys.com/marburg/${DEFAULT_BAG}


# source the setup file to make our installed software available
source ${WORK_DIR}/devel/setup.bash

if [ -n "$1" ]; then

    if [[ $1 == "play" ]]; then

        if [ -n "$2" ]; then
            # Play the user-supplied bag
            # 
            $BAG = $2
        else
            # Download and play the default bag

            BAG=/bags/$DEFAULT_BAG

            ## Use the default bag.
            if [ ! -f $BAG ]; then

                echo "Downloading $BAG"
                curl -o $BAG $DOWNLOAD_LOCATION
                rosbag decompress $BAG

            fi
        fi

        if [ ! -f $BAG ]; then
            echo "Hm, can't find $BAG"
            exit -1
        fi

        echo "Playing $BAG"

        roslaunch trisect_emulator trisect_emulator.launch &
        sleep 2

        rosbag play --loop $BAG

    else
        exec "$@"
    fi

else

    echo "Commands to this docker image:"
    echo "  play              Play the default bagfile.  Will download the bag if necessary."
    echo "  play [bagfile]    Play the specified bagfile from the /bags directory in the container"
    echo "  (any thing else)  Run the specified command in the container"

fi