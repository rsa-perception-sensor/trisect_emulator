ARG ROSDISTRO=noetic
FROM ros:${ROSDISTRO}-ros-base
ARG ROSDISTRO

ARG WORK_DIR=/ros_ws
WORKDIR ${WORK_DIR}

# install tmux and ROSBridge
RUN apt-get update && apt-get install -y \
    tmux python3-pip git \
    ros-${ROSDISTRO}-rosbridge-server \
    ros-${ROSDISTRO}-cv-bridge \
    ros-${ROSDISTRO}-image-transport \
    ros-${ROSDISTRO}-sensor-msgs \
    ros-${ROSDISTRO}-async-web-server-cpp \
    python3-catkin-tools \
    ros-${ROSDISTRO}-roslint \
    curl && \
    rm -rf /var/lib/apt/lists/*

# Install vcstool to check out rostful
RUN pip3 install vcstool jwt

RUN mkdir src
COPY trisect_emulator/ src/trisect_emulator/

# Install rostful from source using the .repos file
RUN vcs import --input src/trisect_emulator/trisect_emulator.repos  --skip-existing src/

RUN . /opt/ros/${ROSDISTRO}/setup.sh && \
    catkin build

VOLUME /bags

ENV WORK_DIR=${WORK_DIR}

# User can set this to point the system to an external roscore
ENV ROS_MASTER_URI=

ADD entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]