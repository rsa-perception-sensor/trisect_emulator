
NAME   := amarburg/trisect_emulator
TAG    := $(shell git log -1 --pretty=%h)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest

DOCKER_CMD=docker run -it --rm -p 8080:8080 -p 8081:8081 -p 8082:8082 

run:
	if [ -d $(BAG_LOCATION) ]; then \
		echo "Mounting $(BAG_LOCATION)"; \
		${DOCKER_CMD} -v ${BAG_LOCATION}:/bags ${LATEST} play; \
	else \
	    ${DOCKER_CMD} ${LATEST} play; \
	fi

#
# Tasks for building the ROS server and cpp client dockers images
#
build:
	docker build . -t ${IMG}
	docker tag ${IMG} ${LATEST}

push: build
	docker push ${IMG}
	docker push ${LATEST}


.phony: build push run